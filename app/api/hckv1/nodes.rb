#!/bin/ruby
require 'grape'

module Hckv1
  class Nodes < Grape::API
    helpers do
      def client
        @cl ||= begin
          Hack::Client.new
        end
      end
    end

    resource 'nodes' do
      params do
        optional :limit, default: 50, type: Integer
        optional :offset, default: 0, type: Integer
      end
      get '/' do
        client.nodes
        # we don't have reference to system yet
        #res = Node.all.sort(limit: [params[:offset], params[:limit]])
        #res.map do |e|
        #  {id:e.node_id, current_robustness: e.system.current_robustness, name:name,max_robustness: e.system.max_robustness,
        #    user_capacity: e.user_capacity,level: e.level, system: e.system.name  }
        #end
        #
        #{"id":271,"current_robustness":0,"name":"sqlite","max_robustness":23,"level":3,"user_capacity":19700,"created_at":"2015-03-21T16:17:17.415+01:00","system_id":11},
      end
    end

    get '/events' do
      client.events
    end

    get '/hackers' do
      res = Actor.all
      res.map do |e|
       # h = {event_id: e.event_id, happened_at: e.happened_at}
       # h[:actor] = e.actor
       # h
        e.attributes
      end
    end

    get '/actors' do
      client.actors
    end

    resource 'traffic' do
      params do
        optional :limit, default: 20, type: Integer
        optional :offset, default: 0, type: Integer
        optional :page, default: 0, type: Integer
      end
      get '/' do
        # for simple pagination
        if params[:page] > 0
          per_page = params[:limit]
          params[:offset] = params[:page] * per_page
        end
        res = Event.all.sort(limit: [params[:offset], params[:limit]])
           #  .order(id: :asc)
        res.map do |e|
         # h = {event_id: e.event_id, happened_at: e.happened_at}
         # h[:actor] = e.actor
         # h
          e.attributes
        end
      end

      params do
        optional :limit, default: 20, type: Integer
        optional :offset, default: 0, type: Integer
        optional :page, default: 0, type: Integer
      end
      get '/events' do
        # for simple pagination
        if params[:page] > 0
          per_page = params[:limit]
          params[:offset] = params[:page] * per_page
        end
        res = Event.all.sort(limit: [params[:offset], params[:limit]])
           #  .order(id: :asc)
        out = {}
        key = 0
        prev = nil
        res.map do |e|
         # h = {event_id: e.event_id, happened_at: e.happened_at}
         # h[:actor] = e.actor
         # h
          time = Time.parse(e.happened_at)
          if time.sec != prev
            key += 1
            out[key] = {}
            out[key][:total] = 0
            out[key][:events] = 0
            out[key][:day] = time.day
            out[key][:month] = time.month
            out[key][:hour] = time.hour
            out[key][:min] = time.min
            out[key][:sec] = time.sec
            out[key][:actions] = {}
            prev = time.sec
          end
          out[key][:actions][e.action.name] ||= 0
          out[key][:actions][e.action.name] += 1
          out[key][:events] += 1
          out[key][:total] += e.robustness.to_i unless e.robustness.nil?
        end
        out
      end

      params do
        optional :limit, default: 20, type: Integer
        optional :offset, default: 0, type: Integer
        optional :page, default: 0, type: Integer
      end
      get '/banner' do
        # for simple pagination
        if params[:page] > 0
          per_page = params[:limit]
          params[:offset] = params[:page] * per_page
        end
        res = Event.all.sort(limit: [params[:offset], params[:limit]])
        out = []
        h = nil
        key = 0
        prev = nil
        max = 0
        group = {}
        res.map do |e|
          time = Time.parse(e.happened_at)
          if !e.system.nil?
            sys = e.system.system_id
            group[sys] ||= {}
            group[sys][:id] = sys
            group[sys][:max] = e.system.max_robustness
            group[sys][:values] ||= []
            group[sys][:values] << {date: e.happened_at, value: e.robustness.to_i}
          end
          if time.sec != prev
            key += 1
            unless h.nil?
              max = h[:value] if h[:value] > max
              out << h
            end
            h = {}
            h[:value] = 0
            h[:events] = 0
            h[:date] = e.happened_at
            h[:actions] = {}
            prev = time.sec
          end
          h[:actions][e.action.name] ||= 0
          h[:actions][e.action.name] += 1
          h[:events] += 1
          h[:value] += e.robustness.to_i unless e.robustness.nil?
        end
        group.map do |k,v|
          v
        end
      end


    end


  end
end