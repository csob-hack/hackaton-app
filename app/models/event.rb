require 'ohm'

class Event < Ohm::Model

  attribute :event_id
  attribute :happened_at
  reference :actor, :Actor
  reference :action, :Action
  reference :node, :Node
  reference :system, :System

  attribute :robustness

  index :event_id

end