require 'ohm'

class Actor < Ohm::Model

  attribute :aid
  attribute :current_action_points
  attribute :goal
  attribute :name
  attribute :doing
  attribute :type
  attribute :actions

  index :aid
end