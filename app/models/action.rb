require 'ohm'

class Action < Ohm::Model

  attribute :action_id
  attribute :name
  attribute :power
  attribute :price


  index :action_id
end

